+++
date = 2020-02-11T07:00:00Z
description = "HOLA MUNDO"
image = "/uploads/SAIT.png"
title = "Cuales son los Cambios Fiscales para el 2020 - SAT MÉXICO"

+++
1. **Arrendamiento**

   En los juicios de arrendamiento de inmuebles, el Juez no autorizará los pagos al arrendador si este no acredita haber expedido los CFDI respectivos; de esta forma el SAT busca evitar la evasión de ISR de los ingresos por arrendamiento
2. **Ventas por Catalogo**

   Para facilitar el cobro del Impuesto sobre la Renta (ISR) en las ventas por catálogo, las empresas mayoristas deberán retener el impuesto para quienes vendan $25 mil pesos al mes o $833 pesos diarios.
3. **Aumento al ISR de ahorro**

   Para 2020 se propone elevar del actual 1.04 por ciento a 1.45 por ciento la tasa de retención por concepto de ISR para las inversiones en el sistema financiero
4. **Entra en vigor 01 Julio 2020 Plataformas Digitales**

   Se establecen requisitos mínimos para facilitar el cumplimiento tributario de las plataformas digitales, incluso para aquellas que no cuenten con establecimiento permanente en el país; a través de un mecanismo de retención de impuestos.
5. **Outsourcing (Subcontratación)**

   Las personas morales o empresa y las personas físicas con actividad empresarial que reciban servicios de subcontratación laboral estarán obligadas a efectuar la retención del IVA
6. **Estrategias Fiscales**

   Los asesores fiscales estarán obligados a reportar sus estrategias contables al SAT o serán multados hasta con un millón de pesos.
7. **Deudas Empresariales**

   Se limitará a las empresas la deducción de intereses derivados de deudas. Se podrá deducir hasta 30 por ciento de los intereses que pagan las empresas en el País a partir de 2020.
8. **Quiebras**

   Cuando una empresa enfrenta un proceso de liquidación, los administradores, socios o accionistas sean responsables solidarios.
9. **Bebidas**

   Se propone incrementar el IEPS sobre bebidas saborizadas a una cuota aplicable de 1.2705 pesos por litro, desde la cuota actual de 1.17 pesos.

   <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
   <!-- Sait v2 infeed BLOGS -->
   <ins class="adsbygoogle"
   style="display:block"
   data-ad-client="ca-pub-9991325280594847"
   data-ad-slot="5771461344"
   data-ad-format="auto"
   data-full-width-responsive="true"></ins>
   <script>
   (adsbygoogle = window.adsbygoogle || \[\]).push({});
   </script>