---
title: "Contacto"
date: 2019-05-12T12:14:34+06:00
description: "This is meta description."
author_image : "images/about/logo_vertical.png"
---

**SAIT Software** es una empresa mexicana nacida en 1991 en el Tecnológico de Monterrey, que surge a raíz de la participación en el “Programa Emprendedor”, con la finalidad de producir software para mejorar la administración en las empresas, desde entonces evolucionó y maduró para convertirse en **MICROSISTEMAS SAN LUIS, S.A. DE C.V.** que inició operaciones formalmente en el año 2000.

El profesionalismo y la calidad de los productos que hemos desarrollado en estos casi 20 años en el mercado, nos han llevado a ser una de las empresas de mayor aceptación en desarrollo de software administrativo, contamos con la preferencia de  un gran numero de  clientes en todo el país que basan sus operaciones en sus productos **SAIT Software Administrativo.**
