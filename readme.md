


**Flujo de trabajo**

```mermaid
graph LR;
    Developer--> Gitlab;
    Forestry --> Gitlab;
    Gitlab -- Mirror --> PreviewServer;
    Developer --> PreviewServer;
    Developer --> ProductionServer;
```

Cuando algun servidor recibe un push ejecuta un script que genera el sitio con HUGO.  
El servicio de forestry se sincroniza con gitlab para hacer push de las publicaciones.  
Gitlab se configura para que el servidor de preview funcione como un mirror, con el fin de que al recibir un cambio de forestry se vea inmediatemente en el server de preview.  
El Developer puede subir sus cambios al repositorio de gitlab y puede subir los cambios directamente al server de preview.  
Cuando todo este correcto se subiran los cambios al server de producción.


## MANEJO DEL SERVIDOR



**Instalaciones iniciales**

Empezamos instalando nginx, git y hugo en el server

Iniciamos sesion como root

    ssh root@new.sait.mx

pswd: pyk52vq


Para instalar  nginx y git simplemente ejecutamos

    $ sudo apt-get update
    $ sudo apt-get install nginx
    $ sudo apt-get install git

Para instalar hugo tenemos que descargarlo del repositorio

    $ wget https://github.com/gohugoio/hugo/releases/download/v0.64.1/hugo_0.64.1_Linux-64bit.deb

Instalamos

    $ sudo dpkg -i hugo_0.64.1_Linux-64bit.deb

Borro archivo

    $ sudo rm hugo_0.64.1_Linux-64bit.deb

Checo la instalación

    $ sudo hugo version


Descargamos los temas de Hugo para sitios que lo requieran

    git clone --recursive https://github.com/spf13/hugoThemes ~/themes

Hay que hacer el cambio en el sitio para que apunte al tema correcto de ser necesario.


**Crear usuario GIT en el server**


    $ sudo adduser git

Me cambio al usuario 'git'

    $ su git
    $ cd
    $ mkdir .ssh && chmod 700 .ssh
    $ touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys

Dar permisos de escritura y lectura al directorio home 

    chmod -R 777 ./



Crear 4 directorios en el home de git, en la seccion de los hooks se explicara su utilidad.

    $ cd /home/git/
    $ mkdir repos/ clone/ public/ backup/ 


**Generar llaves SSH y acceso a gitlab**

Generar llave SSH y copiarla al usuario en gitlab.com

    $ cd ~/.ssh/
    $ ssh-keygen -t rsa -b 4096 -C "git@new.sait.mx"


Copiar el contenido de la llave generada id_rsa.pub a las llaves de nuestro usuario en gitlab para tener permisos en caso de ser requerido para descargar el repositorio.


**Clonar repositorio dentro de /home/git/repos**

Importante usar --bare para no tener problemas con la posición de HEAD

    git clone --bare git@gitlab.com:sait/saitmx.git

Los repositorios clonados con --bare no tienen working directory solo guardan los archivos de cambios por lo que no hay que estar haciendo pull, el inconveniente de esto es que hugo no puede trabajar directamente sobre este directorio, lo solucionaremos mas adelante. 


**CAMBIAR RUTA EN NGINX**

Abrimos el archivo de configuración default de nginx

    $ nano /etc/nginx/sites-available/default

Cambiamos la variable root y server_name por nuestros datos.
en root colocamos '/home/git/public/' que es donde hugo estará generando los sitios estáticos.

    server {
            listen 80 default_server;
            listen [::]:80 default_server ipv6only=on;
    
            root /home/git/public/saitmx;
            index index.html index.htm;
    
            # Make site accessible from http://localhost/
            server_name new.sait.mx;
    
    . . .

Aplicar cambios

    $ sudo service nginx restart


si entramos a new.sait.mx nos dara un mensaje de nginx

**DEPLOY CON HOOKS**


Vamos a crear un HOOK que ejecutara instrucciones después de recibir un push.

    $ cd /home/git/repos/saitmx.git/hooks
    
    $ nano post-receive

Al principio de la guia se crearon 4 directorios, los cuales son para este propósito.
	

 - **repos/** : En este directorio van los repositorios bajados de gitlab.
 - **clone/** : Clone del repositorio '**-bare**' a un **working directory**.   Los repositorios son '--bare' por lo que debemos clonarlos en otro directorio para que se genere el working three y hugo tenga los archivos .md para trabajar.
 -  **public/** : lugar a donde hugo va a guardar los archivos estáticos de nuestro sitio listos para ser servidos por nginx.
 - **backup/** : cada que se ejecute el hook se guardara una copia del sitio y se restablecera automáticamente si algo sale mal. 

El Hook:

 - Borra de Clone/ la versión anterior de los archivos del sitio.
 - Hace un backup del sitio.
 - Crea una regla para atrapar cualquier error y reestablecer el backup.
 - "Clona" el repositorio a un working directory para que hugo trabaje con el.
 - Elimina el contenido del directorio public del sitio para volver a generar el sitio.
 - Ejecuta Hugo indicando el directorio del clon, el directorio donde guardar el sitio estatico y el dominio del sitio.
 - Al finalizar borra el directorio clonado de clone/

Cambiar las variables iniciales por las rutas a los directorios del sitio a servir.


    #!/bin/bash
    
    GIT_REPO=$HOME/repos/saitmx.git
    WORKING_DIRECTORY=$HOME/clone/saitmx
    PUBLIC_WWW=$HOME/public/saitmx
    BACKUP_WWW=$HOME/backup/saitmx
    MY_DOMAIN=new.sait.mx
    
    set -e
    
    rm -rf $WORKING_DIRECTORY
    rsync -aqz $PUBLIC_WWW/ $BACKUP_WWW
    trap "echo 'A problem occurred.  Reverting to backup.'; rsync -aqz --del $BACKUP_WWW/ $PUBLIC_WWW; rm -rf $WORKING_DIRECTORY" EXIT
    
    git clone $GIT_REPO $WORKING_DIRECTORY
    rm -rf $PUBLIC_WWW/*
    /usr/local/bin/hugo -s $WORKING_DIRECTORY -d $PUBLIC_WWW -b "http://${MY_DOMAIN}"
    rm -rf $WORKING_DIRECTORY
    trap - EXIT

Tener cuidado con la antepenúltima linea donde se indica el directorio donde se encuentra Hugo ya que puede variar, se puede encontrar el directorio ejecutando.

    $ whereis hugo
    

**PERMISOS DE DEL USUARIO GIT**

El hook no funcionara si no se tienen los permisos adecuados.

cambio al usuario 'root' y me posiciono en /home/git/ 

    $ su root
    $ cd /home/git

asegurarse que el Dueño de las carpetas es el usuario 'git'

    chown -R git:git backup/ public/ clone/ repos/

cambio el permiso al directorio del usuario git para que no pida pass la sesión ssh y no permitir escritura a externos.

    chmod 775 git/ 

dar permiso de ejecución al hook

    chmod 775 git/saitmx.git/hooks/post-recieve


Me cambio al usuario 'git' y ejecutamos el archivo de hook

    $ su git
    $ bash /home/git/repos/sait.mx/hooks/post-receive

Si todo salio correcto nos indicara 'done.'



**Registrar repositorio espejo en gitlab para subir cambios de foresty.io.**

forestry se enlaza al repositorio en gitlab para estar subiendo los cambios a las publicaciones, lo que haremos sera que cada que llegue un push al repositorio en gitlab se suba a nuestro server.

1. En gitlab entramos al repositorio del proyecto, el usuario debe tener permiso de mantainer.
2. Navega al apartado Settings > Repository y abre la pestaña  Mirroring repositories
3. Agrega la url del repositorio 'ssh://git@new.sait.mx/home/git/repos/saitmx.git'
4. Seleccionar Push en el dropdown de Mirrot direction
5. Seleccionar SSH como metodo de autentificacion
6. Marcar Check box de Only mirror protected branches (hace mas rápido el push). 
7. Click en Mirror Repository

Se agregara el Mirror a una lista.

Del repositorio en la lista hacer clic en el botón para copiar la llave SSH, la vamos a copiar al servidor de new.sait.mx para que acepte los push de gitlab.


en el server dirigirnos a 

    $ cd /home/git/.ssh/
    
    $ nano authorized_keys

agregar la llave en una nueva linea y guardar.



**Subir cambio directamente al server** 

Para que el usuario suba cambios directamente al servidor debe seguir los siguientes pasos

copiar la llave ssh de su maquina al servidor con el comando

    $ ssh-copy-id git@new.sait.mx
agregar el repositorio remoto locamente

    $ git remote add preview  ssh://git@new.sait.mx/home/git/repos/saitmx.git

Para subir algún cambio se hará el push a ese remote.

    $ git push preview master


**Instalar certificado SSL**

Añadir el repositorio de certbot

    $ sudo apt-get update
    $ sudo apt-get install software-properties-common
    $ sudo add-apt-repository universe
    $ sudo add-apt-repository ppa:certbot/certbot
    $ sudo apt-get update

Instalar certbot con plugin para nginx

    $ sudo apt-get install certbot python-certbot-nginx

Ejecutar certbot 

    $ sudo certbot --nginx

Hacer toda la instalacion por defecto, cuando pregunte si deseas redirigir seleccionar la opcion '2' que si queremos redirigir a https.

Hay que hacer un cambio al archivo del hook

    $ nano /home/git/repos/saitmx.git/hooks/post-receive

en la siguiente linea
    /usr/local/bin/hugo -s $WORKING_DIRECTORY -d $PUBLIC_WWW -b "http://${MY_DOMAIN}"

Cambiar el htttp a https
    /usr/local/bin/hugo -s $WORKING_DIRECTORY -d $PUBLIC_WWW -b "https://${MY_DOMAIN}"

y probamos el hook con el usuario git para regenerar el sitio con https.
    bash /home/git/repos/saitmx.git/hooks/post-receive



**Referencias**

https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server  
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-hugo-a-static-site-generator-on-ubuntu-14-04  
https://www.digitalocean.com/community/tutorials/how-to-deploy-a-hugo-site-to-production-with-git-hooks-on-ubuntu-14-04  
https://www.digitalocean.com/community/tutorials/como-configurar-las-llaves-ssh-en-ubuntu-18-04-es  
https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account  
https://stackoverflow.com/questions/21691202/how-to-create-file-execute-mode-permissions-in-git-on-windows  
https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pushing-to-a-remote-repository-core  
https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx


